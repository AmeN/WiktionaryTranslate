create table `wikitionary`.dictionary
(
	id INT not null auto_increment primary key,
	lang VARCHAR(255) not null,
	word_pl VARCHAR(255) not null,
	word_translation VARCHAR(255) not null,
	word_translation_ipa VARCHAR(255) not null
)