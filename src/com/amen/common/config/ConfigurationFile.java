package com.amen.common.config;

import com.amen.common.ApplicationRuntime;
import com.typesafe.config.ConfigFactory;

import java.io.File;

import static com.amen.common.log.Log.Info;

/**
 * Created by AmeN on 16.02.2016.
 */
public class ConfigurationFile {

    private static com.typesafe.config.Config configurationAlternative;
    private static com.typesafe.config.Config configuration;
    private static final String configurationFilename = "config.properties";


    public static void load() {
        String prefix = ApplicationRuntime.getSystemConfiguration().getWorkingDirectory();

        Info(ConfigurationFile.class, "Primary path: " + prefix + configurationFilename);
        configurationAlternative = ConfigFactory.parseFile(new File(prefix + configurationFilename));
        com.typesafe.config.Config fallback = ConfigFactory.parseResources(prefix + "config\\" + configurationFilename);
        configuration = configurationAlternative.withFallback(fallback);
        Info(ConfigurationFile.class, "Backup path: " + prefix + "config\\" + configurationFilename);
    }

    public static boolean hasPath(String path) {
        return configuration.hasPath(path);
    }

    public static boolean getBoolean(String path) {
        return configuration.getBoolean(path);
    }

    public static int getInt(String path) {
        return configuration.getInt(path);
    }

    public static long getLong(String path) {
        return configuration.getLong(path);
    }

    public static double getDouble(String path) {
        return configuration.getDouble(path);
    }

    public static String getString(String path) {
        return configuration.getString(path);
    }

}
