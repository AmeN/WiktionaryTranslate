package com.amen.common.config;

import com.amen.wiktionary.comparer.InternalLanguage;

import java.util.*;

import static com.amen.common.log.Log.Debug;
import static com.amen.common.log.Log.Error;
import static com.amen.common.log.Log.Info;

/**
 * Created by AmeN on 16.02.2016.
 */
public class Configuration {

    public static HashMap<String, InternalLanguage> sLanguages;

    static {
        sLanguages = new HashMap<>();
        String tLangs = ConfigurationFile.getString("languages");
        String[] tShortLanguages = tLangs.split(";");

        for (String tShort : tShortLanguages) {
            InternalLanguage pLang = null;
            try {
                pLang = new InternalLanguage(tShort);
            } catch (Exception e) {
                Error(Configuration.class, "Short name of language " + tShort + ", hasn't been found in the Translation API.");
                continue;
            }
            sLanguages.put(tShort, pLang);
        }
        Info(Configuration.class, "Languages configuration file loaded.");
    }

    public static Collection<InternalLanguage> getLanguages() {
        return sLanguages.values();
    }

    public static Set<String> getShorts() {
        return sLanguages.keySet();
    }
}
