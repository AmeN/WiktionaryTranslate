package com.amen.common.db;

import static com.amen.common.log.Log.Error;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.DriverManager;
import java.sql.SQLException;

public class QuickDb {

    private Connection conn;

    public QuickDb(String hostname, String dbname, String username,
                   String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://"
                    + hostname
                    + ":3306/"
                    + dbname
                    + "?characterEncoding=UTF-8&characterSetResults=UTF-8",
                                                            username, password);
        } catch (Exception ex) {
            Error(getClass(), ex);
        }
    }

    public void executeUpdate(String query) {
        try (PreparedStatement pstmt = (PreparedStatement) conn
                .prepareStatement(query)) {
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Error(getClass(), ex);
        }
    }
}
