package com.amen.common.db;

import static com.amen.common.log.Log.Error;
import static com.amen.common.log.Log.Warn;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Timer;
import org.apache.commons.io.IOUtils;


public final class MySQL {

    //wątek sprawdzający połączenie z bazą, który wykonuje kolejne sprawdzenie bazy
    //po każdym poprawnym rozłączeniu.
    private Timer m_restarting_timer;
    //nazwa bazy danych.
    protected String dbname;
    //nazwa serwera z bazą danych.
    protected String hostname;
    //nazwa użytkownika bazy danych.
    protected String username;
    //hasło do bazy danych.
    protected String password;
    protected Connection conn;

    /**
     * Konstruktor klasy.
     *
     * @param hostname
     * @param dbname
     * @param username
     * @param password
     */
    public MySQL(String hostname, String dbname, String username,
            String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
        }
        this.dbname = dbname;
        this.hostname = hostname;
        this.username = username;
        this.password = password;
    }

    /**
     * Metoda łącząca z bazą danych.
     */
    public void connect() {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
            conn = DriverManager.getConnection("jdbc:mysql://" + hostname
                    + ":3306/" + dbname
                    + "?characterEncoding=UTF-8&characterSetResults=UTF-8",
                    username, password);
        } catch (SQLException ex) {

            Error(getClass(), ex);
        }
    }

    /**
     * Metoda rozłączjąca z bazą danych.
     */
    public void disconnect() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }
    }

    /**
     * Metoda wykonująca zapytanie aktualizujące dane w bazie.
     *
     * @param query
     */
    public boolean executeUpdate(String query) {
        PreparedStatement pstmt = null;
        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                    Error(getClass(),
                            "Thread has been unintentionally interrupted.", ex1);
                }
                executeUpdate(query);
            } else {
                Error(getClass(), ex);
                return false;
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }
        return true;
    }

    /**
     * Metoda pobiera ilość danych pasujących do zapytania.
     *
     * @param query
     *
     * @return int - wynik
     */
    public int getCount(String query) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        int result = 0;

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
            }

            return result;
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getCount(query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }

        return 0;
    }

    /**
     * Metoda pobiera jeden rekord z bazy danych.
     *
     * @param query
     *
     * @return String - wynik
     */
    public String getOne(String query) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String result = null;

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result = rs.getString(1);
            }

            return result;
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getOne(query);
            } else {
                Error(getClass(), ex);
            }
        } catch (NullPointerException npe) {
            disconnect();
            connect();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }

        return null;
    }

    /**
     * Metoda pobiera rząd z bazy danych.
     *
     * @param query
     *
     * @return String[] - tablica wyników
     */
    public String[] getRow(String query) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ResultSetMetaData rsmd;
        String[] result = null;

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            rs = pstmt.executeQuery();
            rsmd = (ResultSetMetaData) rs.getMetaData();
            result = new String[rsmd.getColumnCount()];
            while (rs.next()) {
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    result[i] = rs.getString(i + 1);
                }
            }
            return result;
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getRow(query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }

        return null;
    }

    /**
     * Metoda pobiera rząd z bazy danych.
     *
     * @param query
     *
     * @return HashMap<String, String> - tablica haszująca wyników
     */
    public HashMap<String, String> getRowAssoc(String query) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        ResultSetMetaData rsmd;
        HashMap<String, String> result = new HashMap<String, String>();

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            rs = pstmt.executeQuery();
            rsmd = (ResultSetMetaData) rs.getMetaData();
            while (rs.next()) {
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    result.put(rsmd.getColumnName(i + 1), rs.getString(i + 1));
                }
            }

            return result;
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getRowAssoc(query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }

        }
        return null;
    }

    public List<Map<String, String>> getRowsAssoc(String query) {
        ArrayList<Map<String, String>> list = new ArrayList();
        ResultSet rs = null;

        try {
            try (PreparedStatement pstmt = (PreparedStatement) conn
                    .prepareStatement(query)) {
                ResultSetMetaData rsmd;
                rs = pstmt.executeQuery();
                rsmd = (ResultSetMetaData) rs.getMetaData();
//                rs.beforeFirst();
                while (rs.next()) {
                    HashMap<String, String> data = new HashMap();
                    for (int i = 0; i < rsmd.getColumnCount(); i++) {
                        data
                                .put(rsmd.getColumnLabel(i + 1), rs.getString(i
                                                + 1));
                    }
                    list.add(data);
                }
            }
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getRowsAssoc(query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }

        return list;
    }

    /**
     * Metoda zwraca kolumnę z bazy danych.
     *
     * @param query
     *
     * @return String[][] - dwuwymiarowa tablica wyników
     */
    public String[][] getColumn(String query) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ResultSetMetaData rsmd;
        String[][] result = null;

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            rs = pstmt.executeQuery();
            rs.last();
            rsmd = (ResultSetMetaData) rs.getMetaData();
            result = new String[rs.getRow()][rsmd.getColumnCount()];
            rs.beforeFirst();
            while (rs.next()) {
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    result[rs.getRow() - 1][i] = rs.getString(i + 1);
                }
            }

            return result;
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getColumn(query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }

        return null;
    }

    /**
     * Metoda zwraca kolumnę z bazy danych.
     *
     * @param query
     *
     * @return Integer[][] - dwuwymiarowa tablica wyników
     */
    public Integer[][] getColumnInteger(String query) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        ResultSetMetaData rsmd;
        Integer[][] result = null;

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    query);
            rs = pstmt.executeQuery();
            rs.last();
            rsmd = (ResultSetMetaData) rs.getMetaData();
            result = new Integer[rs.getRow()][rsmd.getColumnCount()];
            rs.beforeFirst();
            while (rs.next()) {
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    result[rs.getRow() - 1][i] = rs.getInt(i + 1);
                }
            }

            return result;
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getColumnInteger(query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (pstmt != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }

        return null;
    }
    private HashMap<InputStream, File> files = new HashMap();

    /**
     * Metoda zwraca strumien do danych.
     *
     * @param p_query Zapytanie, powinno byc tak sformuowane aby otrzymac jeden
     * wynik z jednym polem.
     * <p>
     * @return Strumien do danych.
     */
    public InputStream getOneAsStream(String p_query) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    p_query);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                try {
                    File f = File.createTempFile("Stream", p_query);
                    FileOutputStream fos = new FileOutputStream(f);
                    InputStream dis = rs.getBinaryStream(1);
                    if (dis != null) {
                        IOUtils.copy(dis, fos);
                        dis.close();
                    }
                    fos.close();
                    f.deleteOnExit();
                    FileInputStream fis = new FileInputStream(f);
                    InputStream result = fis != null ? new BufferedInputStream(
                            fis) : null;
                    if (result == null) {
                        f.delete();
                    } else {
                        files.put(result, f);
                    }
                    return result;
                } catch (Exception e) {
                }
            }
        } catch (SQLException ex) {
            Warn(getClass(), ex);
            if (ex.getErrorCode() == 0) {
                connect();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex1) {
                }
                return getOneAsStream(p_query);
            } else {
                Error(getClass(), ex);
            }
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }
        return null;
    }

    public void deleteFileForStream(InputStream is) {
        if (files.get(is) != null) {
            files.get(is).delete();
            files.remove(is);
        }
    }
    /*
     * SetBlob. Metoda która przyjmuje plik do wrzucenia, nazwę tabeli, nazwę kolumny oraz nazwę kolumny
     * po której będziemy identyfikować (w której znajduje się nasz klucz podstawowy).
     *
     */

    public void setBlob(Blob blob, String tableName, String columnName,
            String idColumnName, int id) {
        if (blob == null) {
            return;
        }
        PreparedStatement pstmt = null;
        try {
            pstmt = (PreparedStatement) conn.prepareStatement(
                    "UPDATE `" + tableName + "` SET " + columnName
                    + " = ? WHERE " + idColumnName + " = ?");

            pstmt
                    .setBinaryStream(1, blob.getBinaryStream(), (int) blob
                            .length());
            pstmt.setInt(2, id);

            pstmt.executeUpdate();

        } catch (SQLException ex) {
            Error(getClass(), ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }
    }

    public Blob getBlob(String tableName, String columnName, String idColumnName, int id) {
        Statement pstmt = null;
        ResultSet resultSet = null;
        try {
            pstmt = conn.createStatement();
            pstmt.setQueryTimeout(2);
            resultSet = pstmt.executeQuery("SELECT " + columnName
                    + " as file FROM `"
                    + tableName
                    + "` WHERE "
                    + idColumnName
                    + "='" + id
                    + "'");
            if (resultSet.next()) {
                Blob myBlob = resultSet.getBlob("file");
                if (myBlob != null) {
                    return myBlob;
                } else {
                    return null;
                }
            }

        } catch (SQLException ex) {
            Error(getClass(), ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }
        return null;
    }

    public Blob getBlob(String tableName, String columnName, String idColumnName, String id) {
        Statement pstmt = null;
        ResultSet resultSet = null;
        try {
            pstmt = conn.createStatement();
            pstmt.setQueryTimeout(2);
            resultSet = pstmt.executeQuery("SELECT " + columnName
                    + " as file FROM `"
                    + tableName
                    + "` WHERE "
                    + idColumnName
                    + "='" + id
                    + "'");
            if (resultSet.next()) {
                Blob myBlob = resultSet.getBlob("file");
                if (myBlob != null) {
                    return myBlob;
                } else {
                    return null;
                }
            }

        } catch (SQLException ex) {
            Error(getClass(), ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }

                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (SQLException ex) {
                Error(getClass(), ex);
            }
        }
        return null;
    }

    public Connection getConnection() {
        return conn;
    }

    boolean getTableExists(String dbname, String tableName) {
        try {
            String result = this.getOne("SELECT COUNT(*) FROM information_schema.TABLES WHERE (TABLE_SCHEMA = '" + dbname + "') AND (TABLE_NAME = '" + tableName + "')");
            if (result != null && !result.isEmpty()) {
                return Integer.parseInt(result) > 0;
            }
        } catch (Exception e) {

        }
        return false;
    }
}
