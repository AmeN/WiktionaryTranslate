/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amen.common.db.model;

import java.util.Map;

/**
 *
 * @author AmeN
 */
public class TranslationEntry {

    private int id;
    private String lang;

    private String word_pl;
    private String word_translation;
    private String word_ipa;

    public TranslationEntry() {
        id = -1;
        lang = "-";
        word_pl = "-";
        word_translation = "-";
        word_ipa = "-";
    }

    public TranslationEntry(int id) {
        // download it from db .
    }

    public TranslationEntry(Map<String, String> pMap) {
        id = Integer.parseInt(pMap.get("id"));
        lang = pMap.get("lang");
        word_pl = pMap.get("word_pl");
        word_translation = pMap.get("word_translation");
        word_ipa = pMap.get("word_translation_ipa");
    }


    public int getId() {
        return id;
    }

    public String getLang() {
        return lang;
    }

    public String getWord_pl() {
        return word_pl;
    }

    public String getWord_translation() {
        return word_translation;
    }

    public String getWord_ipa() {
        return word_ipa;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setWord_pl(String word_pl) {
        this.word_pl = word_pl;
    }

    public void setWord_translation(String word_translation) {
        this.word_translation = word_translation;
    }

    public void setWord_ipa(String word_ipa) {
        this.word_ipa = word_ipa;
    }

}
