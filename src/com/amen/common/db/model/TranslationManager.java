/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amen.common.db.model;

import com.amen.common.db.Database;
import com.amen.common.log.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author AmeN
 */
public class TranslationManager {

    public static TranslationManager sManager;

    public TranslationManager() {

    }

    public static TranslationManager getManager() {
        synchronized (TranslationManager.class) {
            if (sManager == null) {
                sManager = new TranslationManager();
            }
        }
        return sManager;
    }

    public List<TranslationEntry> getAllEntries() {
        ArrayList<TranslationEntry> tmpList = new ArrayList();

        List<Map<String, String>> lList = Database.getInstance().getRowsAssoc("SELECT * FROM `dictionary` ");
        lList.stream().map((singleEntry) -> new TranslationEntry(singleEntry)).forEach((pEntry) -> {
            tmpList.add(pEntry);
        });

        return tmpList;
    }

    public void putEntry(TranslationEntry pEntry) {
        Database.getInstance().executeUpdate("INSERT INTO `dictionary` "
                + "(`id`, `lang`, `word_pl`, `word_translation`, `word_translation_ipa`) VALUES "
                + "(NULL,'" + pEntry.getLang() + "','" + pEntry.getWord_pl() + "','" + pEntry.getWord_translation() + "','" + pEntry.getWord_ipa() + "')");

    }

    public void updateEntry(TranslationEntry pEntry) {
        Database.getInstance().executeUpdate("UPDATE `dictionary` SET "
                + " `lang`='" + pEntry.getLang() + "', `word_pl`='" + pEntry.getWord_pl() + "', `word_translation`='" + pEntry.getWord_translation() + "', `word_translation_ipa`='" + pEntry.getWord_ipa() + "' WHERE `id`='" + pEntry.getId() + "'");
    }

    public ArrayList<String> getAllTranslatedWordsFromDB() {
        ArrayList<String> resultWords = new ArrayList<>();

        List<Map<String, String>> pList = Database.getInstance().getRowsAssoc("SELECT DISTINCT `word_pl` FROM `dictionary`");
        for (Map<String, String> lEntry : pList) {
            resultWords.add(lEntry.get("word_pl"));
        }

        return resultWords;
    }

    public ArrayList<TranslationEntry> getAllTranslatedEntriesContainingWord(String pWord) {
        ArrayList<TranslationEntry> resultWords = new ArrayList<>();

        List<Map<String, String>> pList = Database.getInstance().getRowsAssoc("SELECT * FROM `dictionary` WHERE `word_pl`='" + pWord + "'");
        for (Map<String, String> lEntry : pList) {
            resultWords.add(new TranslationEntry(lEntry));
        }

        return resultWords;
    }

    public TranslationEntry getTranslationOfWordInLang(String language, String word) {
        TranslationEntry result = null;
        List<Map<String, String>> pList = Database.getInstance().getRowsAssoc("SELECT * FROM `dictionary` WHERE `lang`='" + language + "' and `word_pl`='" + word + "'");
        if (pList.size() > 0) {
            result = new TranslationEntry(pList.get(0));
        } else {
            result = null;
        }

        if (pList.size() > 1) {
            Log.Info(getClass(), "More translations ?");
        }

        return result;

    }
}
