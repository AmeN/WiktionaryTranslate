/**
 * ******************************
 * Nowatel Sp. z o.o. *
 * Wszelkie prawa zastrzeżone. *
 * 2011 *
 *******************************
 */
package com.amen.common.db;

import static com.amen.common.log.Log.Info;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Klasa służąca jako sterownik bazy danych. Podczas inicjalizacji klasy wybiera
 * się sterownik, jeśli zmieniona zostanie baza danych należy zmienić tylko
 * sterownik przy inicjalizacji.
 *
 * TODO - dorobić wkońcu ten sterownik :)
 *
 * @author Nowatel Sp. z o.o. (Michał Smolik)
 * @version 0.0.1
 * @since 0.0.1
 */
public class Database {

    //nazwa bazy danych.
    protected String dbname;
    //nazwa serwera z bazą danych.
    protected String hostname;
    //nazwa użytkownika bazy danych.
    protected String username;
    //hasło do bazy danych.
    protected String password;
    private MySQL mysql;
    private static volatile Database instance = null;

    /**
     * Singleton.
     *
     * @return singleton klasy
     */
    public static Database getInstance() {
        if (instance == null) {
            synchronized (Database.class) {
                if (instance == null) {
                    instance = new Database();
                }
            }
        }

        return instance;
    }

    /**
     * Metoda łącząca z bazą danych.
     */
    public void connect() {
        if (mysql == null) {
            mysql = new MySQL(hostname, dbname, username, password);
        }

        mysql.connect();
    }

    /**
     * Metoda rozłączająca z bazą danych.
     */
    public void disconnect() {
        if (mysql != null) {
            mysql.connect();
        }
    }

    /**
     * Metoda ustawia dane używane do połączenia się z bazą danych. Metodą
     * należy
     * wywołać orzed metodą connect.
     *
     * @param dbname
     * @param hostname
     * @param username
     * @param password
     */
    public void setConnectionData(String dbname, String hostname,
                                  String username, String password) {
        this.dbname = dbname;
        this.hostname = hostname;
        this.username = username;
        this.password = password;
    }

    /**
     * Metoda wykonująca zapytanie aktualizujące dane w bazie.
     *
     * @param query
     *              <p>
     * @return
     */
    public boolean executeUpdate(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.executeUpdate(query);
    }

    /**
     * Metoda pobiera ilość danych pasujących do zapytania.
     *
     * @param query
     *
     * @return int - wynik
     */
    public int getCount(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getCount(query);
    }

    /**
     * Metoda pobiera jeden rekord z bazy danych.
     *
     * @param query
     *
     * @return String - wynik
     */
    public String getOne(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getOne(query);
    }

    /**
     * Metoda pobiera rząd z bazy danych.
     *
     * @param query
     *
     * @return String[] - tablica wyników
     */
    public String[] getRow(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getRow(query);
    }

    /**
     * Metoda pobiera rząd z bazy danych.
     *
     * @param query
     *
     * @return HashMap<String, String> - tablica haszująca wyników
     */
    public HashMap<String, String> getRowAssoc(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getRowAssoc(query);
    }

    /**
     * Metoda pobiera rząd z bazy danych.
     *
     * @param query
     *
     * @return HashMap<String, String> - tablica haszująca wyników
     */
    public List<Map<String, String>> getRowsAssoc(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getRowsAssoc(query);
    }

    /**
     * Metoda zwraca kolumnę z bazy danych.
     *
     * @param query
     *
     * @return String[][] - dwuwymiarowa tablica wyników
     */
    public String[][] getColumn(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getColumn(query);
    }

    /**
     * Metoda zwraca kolumnę z bazy danych.
     *
     * @param query
     *
     * @return Integer[][] - dwuwymiarowa tablica wyników
     */
    public Integer[][] getColumnInteger(String query) {
        Info(getClass(), "Database query: " + query);
        return mysql.getColumnInteger(query);
    }

    /**
     * Metoda zwraca strumien do danych.
     * <p>
     * @param p_query Zapytanie, powinno byc tak sformuowane aby otrzymac jeden
     *                wynik z jednym polem.
     * <p>
     * @return Strumien do danych.
     */
    public InputStream getOneAsStream(String p_query) {
        return mysql.getOneAsStream(p_query);
    }

    public void deleteFileForStream(InputStream is) {
        mysql.deleteFileForStream(is);
    }

    /*
     * SetBlob. Metoda która przyjmuje plik do wrzucenia, nazwę tabeli, nazwę kolumny oraz nazwę kolumny
     * po której będziemy identyfikować (w której znajduje się nasz klucz podstawowy).
     *
     */
    public void setBlob(Blob blob, String tableName, String columnName,
                        String idColumnName, int id) {
        Info(getClass(), "Executing querry to set Blob field in db. Execution: "
                                 + dbname + "." + tableName + "." + columnName);
        this.mysql.setBlob(blob, tableName, columnName, idColumnName, id);
    }

    public Blob getBlob(String tableName, String columnName, String idColumnName,
                        int id) {
        Info(getClass(),
             "Executing querry to get Blob field from db. Execution: "
                     + dbname + "." + tableName + "." + columnName);
        return this.mysql.getBlob(tableName, columnName, idColumnName, id);
    }

    /**
     * Retrieves Blob from db.
     * <p>
     * @param tableName    - table name.
     * @param columnName   - column name.
     * @param idColumnName - identifying column name.
     * @param id           - identifier retrieved.
     * <p>
     * @return an object of Blob type / null if table contains null.
     */
    public Blob getBlob(String tableName, String columnName, String idColumnName,
                        String id) {
        Info(getClass(),
             "Executing querry to get Blob field from db. Execution: "
                     + dbname + "." + tableName + "." + columnName);
        return this.mysql.getBlob(tableName, columnName, idColumnName, id);
    }

    public Connection getConnection() {
        return mysql.getConnection();
    }
    
    
    public boolean getTableExists(String tableName) {
        return mysql.getTableExists(dbname,tableName);
    }

}
