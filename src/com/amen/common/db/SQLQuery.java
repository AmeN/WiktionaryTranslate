package com.amen.common.db;

import com.amen.common.log.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AmeN on 13.12.2015.
 */
public class SQLQuery {

    private final static String SEPARATOR_STATEMENTS = "%#3";   //between single statements in set
    private final static String SEPARATOR_INTERNAL_DB = "%#2";  //between id, statements and rollbacks
    private final static String SEPARATOR_QRIES = "%4%5";   //between single statements in set

    private String version;
    private final List<String> query;
    private final List<String> queryRollback;

    public SQLQuery(ArrayList<String> lst) {
        query = new ArrayList();
        queryRollback = new ArrayList();

        fromString(lst);
    }

    public SQLQuery(String vsn, List<String> sql) {
        query = new ArrayList(sql);
        queryRollback = new ArrayList();

        version = vsn;
    }

    public SQLQuery(String vsn, String sql, String rback) {
        query = new ArrayList();
        queryRollback = new ArrayList();

        query.addAll(Arrays.asList(sql.split(SEPARATOR_QRIES)));
        queryRollback.addAll(Arrays.asList(rback.split(SEPARATOR_QRIES)));
        version = vsn;
    }

    public List<String> getQuery() {
        return query;
    }

    public List<String> getQueryRollback() {
        return queryRollback;
    }

    public String getQueryStr() {
        return evalList(query);
    }

    private String evalList(List<String> query) {
        StringBuilder bld = new StringBuilder();
        query.stream().map((s) -> {
            bld.append(s);
            return s;
        }).forEach((_item) -> {
            bld.append(SEPARATOR_QRIES);
        });

        return bld.length() > SEPARATOR_QRIES.length() ? bld.substring(0, bld.length() - SEPARATOR_QRIES.length()) : bld.toString();
    }

    public String getQueryRollbackStr() {
        return evalList(queryRollback);
    }

    @Override
    public String toString() {
        return version + SEPARATOR_INTERNAL_DB
                + getQueryStr() + SEPARATOR_INTERNAL_DB
                + getQueryRollbackStr();
    }

    private void fromString(ArrayList<String> textBlock) {
        StringBuilder statement = new StringBuilder();
        boolean rollback = false;
        for (String line : textBlock) {
            if (line.equalsIgnoreCase("///////STATEMENT_DONE////////")) {
                queryRollback.add(statement.toString());
                Log.Info(SQLQuery.class, "Statement R: " + statement.toString());
                break;
                //statement = new StringBuilder();
            } else if (line.startsWith("VERSION=")) {
                version = line.split("=")[1];
            } else if (line.equalsIgnoreCase("///////STATEMENT_RBCK////////")) {                  //rollbackBegin
                rollback = true;
                query.add(statement.toString());
                Log.Info(SQLQuery.class, "Statement Q: " + query);
                statement = new StringBuilder();
            } else if (line.equalsIgnoreCase("//////////STATEMENT//////////")) {
                Log.Info(SQLQuery.class, "Statement nothing...");
            } else if (line.endsWith(";")) {
                statement.append(line.substring(0, (line.length() - 1)));
                statement.append(SEPARATOR_QRIES);
                if (rollback) {
                    queryRollback.add(statement.toString());
                } else {
                    query.add(statement.toString());
                }
                statement = new StringBuilder();
            } else {
                statement.append(line);
            }
        }
    }
}
