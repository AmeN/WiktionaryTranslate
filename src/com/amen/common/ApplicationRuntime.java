/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amen.common;

import com.amen.common.config.ConfigurationSystem;

import static com.amen.common.log.Log.Fatal;
import static com.amen.common.log.Log.Error;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AmeN
 */
public class ApplicationRuntime {

    private static Boolean sIsWorking;
    private static final ConfigurationSystem sSysConfig;
    private static final Map<String, ApplicationParameter> sParameters;
    //

    private static final String applicationDirectory;

    static {
        sIsWorking = true;
        sSysConfig = new ConfigurationSystem();
        String pathToAudioFolder = sSysConfig.getWorkingDirectory() + "/audio/";

        File audioFolder = new File(pathToAudioFolder);
        if (!audioFolder.exists() || audioFolder.isDirectory()) {
            audioFolder.mkdir();
        }

        sParameters = new HashMap<>();
        sParameters.put("--configFile", new ApplicationParameter("--configFile"));
        sParameters.put("--words", new ApplicationParameter("--words"));

        applicationDirectory = sSysConfig.getWorkingDirectory();
        try {
            if (Files.notExists(Paths.get(applicationDirectory), LinkOption.NOFOLLOW_LINKS)) {
                Files.createDirectory(Paths.get(applicationDirectory));
            }
        } catch (IOException ex) {
            Fatal(ApplicationRuntime.class, "Application directory can not be created!", ex);
        }
    }

    public static ConfigurationSystem getSystemConfiguration() {
        return sSysConfig;
    }

    public static void parseApplicationParameters(String[] args) {
        List<String> argsProvided = Arrays.asList(args);

        argsProvided.stream().forEach((parameter) -> {
            try {
                if (!parameter.contains("=")) {
                    throw new Exception(String.format("Argument \"%s\"unrecognized.", parameter));
                }
                if (sParameters.containsKey(parameter.split("=")[0])) {
                    sParameters.get(parameter.split("=")[0]).setValue(parameter.split("=")[1]);
                }
            } catch (Exception e) {
                Error(ApplicationRuntime.class, e);
            }
        });
    }

    public static String getAlternativeConfig() {
        return sParameters.get("--config").getValue();
    }
    
    public static String getWords() {
        return sParameters.get("--words").getValue();
    }

    public static String getAppDirectory() {
        return applicationDirectory;
    }

    public static void stopRunning() {
        sIsWorking = false;
    }


}
