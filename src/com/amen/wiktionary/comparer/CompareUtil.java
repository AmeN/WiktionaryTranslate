/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amen.wiktionary.comparer;

import static com.amen.common.log.Log.Debug;
import static com.amen.common.log.Log.Info;

/**
 *
 * @author AmeN
 */
public class CompareUtil {

    public static double evaluateSimilarity(String first, String second) {
        int stringSize1 = first.length();
        int stringSize2 = second.length();

        Double pointMaximum = 0.0;
        Double pointsGathered = 0.0;

        int combination = 0;
        for (int letter1 = 0; letter1 < stringSize1 - combination; letter1++) {
            if (second.length() >= letter1 + combination + 1) {

                String sub1 = first.substring(letter1, letter1 + combination + 1).toLowerCase();
                String sub2 = second.substring(letter1, letter1 + combination + 1).toLowerCase();
                String sub2Next = null;
                try {
                    sub2Next = second.substring(letter1 + 1, letter1 + combination + 2).toLowerCase();
                } catch (Exception e) {
                }
                String sub2Prev = null;
                try {
                    sub2Prev = second.substring(letter1 - 1, letter1 + combination - 1).toLowerCase();
                } catch (Exception e) {
                }

                pointMaximum += 1;
                if (sub1.equals(sub2)) {
                    pointsGathered += 1;
                } else if ((sub2Next != null && sub1.equals(sub2Next)) || (sub2Prev != null && sub1.equals(sub2Prev))) {
                    pointsGathered += 0.5;
                }
            }
        }

        return ((double) pointsGathered / (double) pointMaximum) * 100.0;
    }
}
