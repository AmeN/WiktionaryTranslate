package com.amen.wiktionary.comparer;

import com.amen.common.ApplicationRuntime;
import com.amen.common.db.Database;
import com.amen.common.db.model.TranslationEntry;
import com.amen.common.db.model.TranslationManager;
import org.apache.commons.io.FileUtils;
import org.google.translate.api.v2.core.model.Language;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.zip.ZipException;

import static com.amen.common.log.Log.*;
import java.net.MalformedURLException;
import java.util.Map;

/**
 * Class is representing single word, and it's information extracted from available websites. Created by AmeN on 16.02.2016.
 */
public class WordTranslation {

    private final static String ENGLISH = "en", POLISH = "pl";
    private final static HashMap<String, Integer> sUnaccessibleLanguages = new HashMap<>();

    // ipa string
    private String mIPA;
    // word itself - or rather pronunciation of our word in given language
    private String mWord;
    // file with audio pronunciation
    private File mSound;
    // language in which the pronunciation was looked for
    private Language mLang;
    // language in which the pronunciation was looked for [internal class - not from the mediawiki library]
    private InternalLanguage mLangInternal;

    /**
     * Constructs object with translation information. Final stage is to search for it's pronunciation on websites.
     *
     * @param pWord - translation of word.
     * @param pLang - language in which we translated it.
     * @param pLangInternal - language in which we translated it.
     */
    public WordTranslation(String pWord, Language pLang, InternalLanguage pLangInternal) {
        mWord = pWord;
        mLang = pLang;
        mLangInternal = pLangInternal;
    }

    public WordTranslation(String pWord, Language pLang, InternalLanguage pLangInternal, String ipa) {
        mWord = pWord;
        mLang = pLang;
        mLangInternal = pLangInternal;
        mIPA = ipa;
    }

    /**
     * Downloads and sets all the necessary data from the Internet.
     * <p>
     * Doing it here, because if we do it in the constructor, than we can't monitor it.
     */
    public void acquireData() {
        findPronunciations(mLang.getLanguage(), mWord);
        mIPA = requestIPA();
    }

    /**
     * Getter for language.
     *
     * @return language name string.
     */
    public String getLanguage() {
        return mLang.toString();
    }

    /**
     * Getter for word.
     *
     * @return word string.
     */
    public String getWord() {
        return mWord;
    }

    /**
     * Getter for IPA.
     *
     * @return IPA string.
     */
    public String requestIPA() {
        return mIPA == null ? mIPA : mIPA.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("/", "");
    }

    /**
     * Getter for audio file with pronunciation.
     *
     * @return file with audio pronunciation.
     */
    public File getAudio() {
        return mSound;
    }

    public Language getmLang() {
        return mLang;
    }

    /**
     * Method checks the given wiktionary language url and tries to access word information from the website. It may crash, but than we try to access it
     * different way [by mediawiki API].
     *
     * @param pLanguage - language in which we try to access the wiktionary. Null if website can't be accessed.
     * @param pWord - word we are looking for.
     * @return Document containing html structure of the website.
     * @throws IOException - may be thrown if any unexpected error occur when parsing document.
     */
    private Document getWebsiteDocument(String pLanguage, String pWord) throws Exception {
        String websiteFinalURL = new String("https://" + pLanguage + ".wiktionary.org/wiki/" + URLEncoder.encode(pWord, "UTF-8"));
        try {
            Debug(getClass(), "Trying to connect: " + websiteFinalURL);
            return Jsoup.connect(websiteFinalURL).get();
        } catch (HttpStatusException e) {
            if (e.getStatusCode() == 404) {
                Warn(getClass(), "Website is unavailable, can't be found: " + websiteFinalURL);

                if (!(pLanguage.equals(ENGLISH) || pLanguage.equals(POLISH))) {
                    Integer unaccessibleCount = sUnaccessibleLanguages.get(pLanguage);
                    if (unaccessibleCount != null) {
                        sUnaccessibleLanguages.remove(pLanguage);
                        sUnaccessibleLanguages.put(pLanguage, unaccessibleCount + 1);
                    } else {
                        sUnaccessibleLanguages.put(pLanguage, 1);
                    }
                }
            } else {
                Warn(getClass(), "Http error [" + websiteFinalURL + "]: " + e.getMessage(), e);
            }
        }

        Debug(getClass(), "No success getting the document, trying to access API.");
        Document doc = null;
        try {
            doc = Jsoup.parse(requestAPIPageContent(pLanguage, pWord));
        } catch (Exception e) {
            Warn(getClass(), "Can't parse wiki API content.");
        }
        if (doc != null) {
            Elements el = doc.getElementsByClass("IPA");
            if (el.size() == 0) {
                Warn(getClass(), "No elements named IPA found in language " + pLanguage + " for word " + pWord);
            } else {
                return doc;
            }
        }
        return null;
    }

    /**
     * Method is trying to access WikiMedia API to download page html content. If found will be returned, if not - null will be returned.
     *
     * @param pLang - language in which te word will be looked for. Available are the WikiMedia languages.
     * @param pWord - key word to look for.
     * @return proper string value with HTML content, or null if page is unavailable.
     */
    private String requestAPIPageContent(String pLang, String pWord) {
        String str = null;
        try {
            return new org.wikipedia.Wiki(pLang + ".wiktionary.org").getRenderedText(pWord);
        } catch (ZipException zipException) {
            Error(getClass(), "Wrong data format for wiki API.");
            return null;
        } catch (Exception e) {
            Error(getClass(), "Unable to parse data received from wrapper.", e);
        }
        return str;
    }

    /**
     * Method is looking for the website under given address. After that if if website is accessible, than it's content is being parsed to get the structure
     * information of the website. This structure can be further parsed to extract any information from it. In this particular case we are looking for any
     * element of class 'IPA'.
     *
     * @param pLanguage - language in which we are trying to access the website.
     * @param pWord - word to look for on the website.
     * @return - document containing html structure of the website.
     * @throws Exception - may be thrown if any error occurs when connecting to the website or parsing the data.
     */
    private Document findDocument(String pLanguage, String pWord) throws Exception {
        Debug(getClass(), "Downloading website in " + pLanguage);

        Document returnDocument = getWebsiteDocument(pLanguage, pWord);

        Elements elements = returnDocument.getElementsByClass("IPA");
        if (elements.size() == 0) {
            Warn(getClass(), "No IPA element found for " + pLanguage + " " + pWord);
        } else {
            return returnDocument;
        }
        elements = returnDocument.getElementsByClass("ipa");
        if (elements.size() == 0) {
            Warn(getClass(), "No ipa element found for " + pLanguage + " " + pWord);
        } else {
            return returnDocument;
        }
        elements = returnDocument.getElementsByClass("AFI");
        if (elements.size() == 0) {
            Warn(getClass(), "No AFI element found for " + pLanguage + " " + pWord);
        } else {
            return returnDocument;
        }
        return null;
    }

    /**
     * Method is using given websites in 3 possible languages, the main language of the country, English website, and Polish. After downloading some document,
     * we search through it's elements to find us information about IPA, and audio file url.
     *
     * @param pLanguage - language in which we try to access the wiktionary. Null if website can't be accessed.
     * @param pWord - word we are looking for.
     */
    private void findPronunciations(String pLanguage, String pWord) {
        Document foundHTMLDocument = null;
        try {
            try {
                foundHTMLDocument = findDocument(pLanguage, pWord);
                if (foundHTMLDocument == null) {
                    throw new Exception("Didn't found " + pWord + ", switching language from " + pLanguage);
                }
            } catch (Exception exBase) {
                Warn(getClass(), pWord + " not found in lang: " + pLanguage);
                // not found in base lang.
                try {
                    foundHTMLDocument = findDocument(ENGLISH, pWord);
                    if (foundHTMLDocument == null) {
                        throw new Exception("Didn't found " + pWord + ", switching language from " + ENGLISH);
                    }
                } catch (Exception exEnglish) {
                    Warn(getClass(), pWord + " not found in lang: " + ENGLISH);
                    // not found in en.
                    try {
                        foundHTMLDocument = findDocument(POLISH, pWord);
                        if (foundHTMLDocument == null) {
                            throw new Exception("Didn't found " + pWord + ", switching language from " + POLISH);
                        }
                    } catch (Exception exPolish) {
                        Warn(getClass(), pWord + " not found in lang: " + POLISH);
                        // not found in pl.
                        foundHTMLDocument = null;
                    }
                }
            }

            if (foundHTMLDocument != null) {
                Debug(getClass(), "Looking for element that could contain IPA (pronunciation).");
                Elements elements = foundHTMLDocument.getElementsByClass("IPA");
                for (Element e : elements) {
                    mIPA = e.text();
                    break;
                }
            } else {
                Debug(getClass(), "No document found, no dictionary contains word: " + pWord + ".");
                mIPA = null;
            }

            if (foundHTMLDocument != null) {
                Debug(getClass(), "Looking for any element that could contain audio file url.");
                String url = null;
                // Looking for href(link) element, it has to be link to audio.
                Elements allLinks = foundHTMLDocument.getElementsByAttribute("href");
                for (Element link : allLinks) {
                    if (link.hasAttr("href")) {
                        String attrValue = link.attr("href");
                        // checking if file has the right extension. We accept mp3, wav, ogg files.
                        if (attrValue.endsWith("ogg") || attrValue.endsWith("mp3") || attrValue.endsWith("wav")) {
                            // checking if it is on the right place
                            if (attrValue.startsWith("//")
                                    && attrValue.contains("org")
                                    && attrValue.startsWith("//upload.wikimedia.org/wikipedia/commons/")) {
                                //checking if that file is online [link is active]
//                                if (Jsoup.connect("https:" + attrValue).get() != null) {
                                // and now we are ok.
                                url = attrValue;
                                try {
                                    if (download(url)) {
                                        break;
                                    } else {
                                        mSound = null;
                                        continue;
                                    }
                                } catch (Exception e) {
                                    Error(getClass(), "Unable to play.");
                                    mSound = null;
                                    continue;
                                }
//                                } else {
//                                    Debug(getClass(), "Link[" + link + "] is unavailable (offline).");
//                                }
                            } else {
                                Debug(getClass(), "Link[" + link + "] is not on the right server.");
                            }
                        } else {
                            Debug(getClass(), "Link[" + link + "] does not contain proper file type.");
                        }
                    } else {
                        Debug(getClass(), "Link[" + link + "] does not have the attribute href.");
                    }
                }
                // not, that we have the right link, let'g go and download it.
            }
        } catch (Exception e) {
            Error(getClass(), "An Error ocurred while trying to find pronounciation values for word: " + mWord, e);
            return;
        }
        Info(getClass(), "Finished looking for pronunciation information for word: " + pWord);
    }

    public boolean download(String url) throws MalformedURLException, IOException, Exception {
        Debug(getClass(), "Found proper audio url, downloading it from: https:" + url);
        File fileOnDrive = new File(ApplicationRuntime.getSystemConfiguration().getWorkingDirectory() + "/audio/" + url.split("/")[url.split("/").length - 1]);
        if (fileOnDrive.exists()) {
            Warn(getClass(), "File with the same name already exists: " + fileOnDrive.getAbsolutePath());
            if (!fileOnDrive.delete()) {
                Warn(getClass(), "Couldn't delete file: " + fileOnDrive.getAbsolutePath());
            } else {
                Debug(getClass(), "Deleted file with the same name: " + fileOnDrive.getAbsolutePath());
            }
        }
        URL downloadSite = new URL("https:" + url);
        FileUtils.copyURLToFile(downloadSite, fileOnDrive);

        Debug(getClass(), "File has been downloaded. It contains pronounciation of the word " + mWord);

        mSound = fileOnDrive;
        return AudioFilePlayer.getPlayer().tryConvert(fileOnDrive.getAbsolutePath());
    }

    public void insertOrUpdate(String pBasePLWord) {
        Map<String, String> pValues = Database.getInstance().getRowAssoc("SELECT * FROM `dictionary` WHERE `word_pl`='" + pBasePLWord + "' and `lang`='" + mLang.getLanguage() + "'");

        TranslationEntry pEntry;
        if (pValues != null && pValues.size() > 0) {
            pEntry = new TranslationEntry(pValues);
        } else {
            pEntry = new TranslationEntry();
        }

        pEntry.setLang(mLang.getLanguage());
        pEntry.setWord_pl(pBasePLWord);
        pEntry.setWord_ipa(mIPA);
        pEntry.setWord_translation(mWord);

        if (pValues != null && pValues.size() > 0) {
            TranslationManager.getManager().updateEntry(pEntry);
        } else {
            TranslationManager.getManager().putEntry(pEntry);
        }
    }
}
