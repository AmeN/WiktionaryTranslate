package com.amen.wiktionary.comparer;

import com.amen.common.config.Configuration;
import com.amen.common.log.Log;
import org.google.translate.api.v2.core.model.Language;

import java.util.ArrayList;
import com.amen.wiktionary.comparer.gui.ChangeListener;
import java.util.Collection;
import static com.amen.common.log.Log.Debug;
import static com.amen.common.log.Log.Error;

/**
 * Represents word. Created by AmeN on 16.02.2016.
 */
public class Word {

    private String mWord;
    private String mIPA;
    private WordTranslation mMe;
    private ArrayList<WordTranslation> mTranslations;

    /**
     * Creates an object representation of word
     *
     * @param pWord - given word.
     */
    public Word(String pWord) {
        mWord = pWord;
        mTranslations = null;
    }

    public void findIPA(Language pLang, InternalLanguage pInternalLanguage) {
        mMe = new WordTranslation(mWord, pLang, pInternalLanguage);
        mMe.acquireData();
        mIPA = mMe.requestIPA();
    }

    public String getIPA() {
        return mIPA;
    }

    /**
     * Executes querries for all languages given in the configuration file.
     *
     * @return map with mappings: language name abbreviation is mapped to it's translation.
     */
    public ArrayList<WordTranslation> getTranslations(ChangeListener pListener) {
        try {
            Collection<InternalLanguage> tmpLangs = Configuration.getLanguages();

            Log.Info(getClass(), "Languages to operate with: " + tmpLangs);
            if (mTranslations == null) {
                mTranslations = new ArrayList<>();
                Log.Info(getClass(), " " + Configuration.getLanguages());
                int iterator = 0;
                if (pListener != null) {
                    pListener.show("Tłumaczę słowo w wybranych językach...");
                }
                for (InternalLanguage tInternalLanguage : Configuration.getLanguages()) {
                    mTranslations.add(tInternalLanguage.translate(mWord));
                    if (pListener != null) {
                        pListener.show((int) ((double) ((double) (++iterator) / (double) tmpLangs.size()) * 100.0));
                    }
                }
                if (pListener != null) {
                    pListener.show("Zakończono tłumaczenie.");
                    pListener.show(0);
                }
            }
        } catch (Exception e) {
            Error(getClass(), e);
        }
        return mTranslations;
    }

    public String getWord() {
        return mWord;
    }
}
