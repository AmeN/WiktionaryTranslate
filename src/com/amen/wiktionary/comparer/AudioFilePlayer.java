package com.amen.wiktionary.comparer;

import com.amen.common.log.Log;
import static com.amen.common.log.Log.Error;
import static com.amen.common.log.Log.Warn;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import static javax.sound.sampled.AudioSystem.getAudioInputStream;
import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

/**
 * Represents audio Player. Created by AmeN on 19.02.2016.
 */
public class AudioFilePlayer {

    private static AudioFilePlayer sPlayer = null;

    /**
     * Getter fir statuc object.
     *
     * @return static player.
     */
    public static synchronized AudioFilePlayer getPlayer() {
        if (sPlayer == null) {
            sPlayer = new AudioFilePlayer();
        }
        return sPlayer;
    }

    public static void javaFlacEncoderPlay(String path) {

    }

    /**
     * Plays given audio file.
     *
     * @param filePath - path to playable file.
     */
    public void play(String filePath) {
        final File file = new File(filePath);

        try (final AudioInputStream in = getAudioInputStream(file)) {

            // creating audio format.
            final AudioFormat outFormat = getOutFormat(in.getFormat());
            final Info info = new Info(SourceDataLine.class, outFormat);

            // creating data line
            try (final SourceDataLine line
                    = (SourceDataLine) AudioSystem.getLine(info)) {

                if (line != null) {
                    line.open(outFormat);
                    line.start(); // starting play
                    try {
                        stream(getAudioInputStream(outFormat, in), line);
                    } catch (Exception e) {
                        Log.Warn(AudioFilePlayer.class, "Error : " + e);
                        //playSound(filePath);
                    }
                    line.drain();
                    line.stop(); // stopping
                }
            }

        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Parses given Audioformat and returns new tranformed audio format.
     *
     * @param inFormat - input format.
     * @return transformed format.
     */
    private AudioFormat getOutFormat(AudioFormat inFormat) {
        final int ch = inFormat.getChannels();
        final float rate = inFormat.getSampleRate();
        return new AudioFormat(PCM_SIGNED, rate, 16, ch, ch * 2, rate, false);
    }

    /**
     * Writing Audio input stream to the SourceDataLine.
     *
     * @param in - input stream
     * @param line - source output.
     * @throws IOException - whenever an error occurs reading or writing to any
     * of the streams.
     */
    private void stream(AudioInputStream in, SourceDataLine line)
            throws IOException {
        final byte[] buffer = new byte[65536];
        for (int n = 0; n != -1; n = in.read(buffer, 0, buffer.length)) {
            line.write(buffer, 0, n);
        }
    }

    public boolean tryConvert(String filePath) throws Exception {
        File file = new File(filePath);
        try {
            AudioInputStream in = getAudioInputStream(file);
            getAudioInputStream(getOutFormat(in.getFormat()), in);
        } catch (IllegalArgumentException e) {
            Warn(getClass(), "Cant use this file. I have no library that could decode this file.");
            return false;
        } catch (Exception e) {
            Error(getClass(), "Error while trying to parse audio file.", e);
            return false;
        }
        return true;
    }

//    private void tryPlay(final File file) throws Exception {
//        try {
//            Clip clip = AudioSystem.getClip();
//            try (final AudioInputStream in = getAudioInputStream(file)) {
//                clip.open(in);
//                clip.start();
//                clip.loop(1);
//                clip.stop();
//                clip.close();
//            }
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//        }
//    }
}
