package com.amen.wiktionary.comparer;

import com.amen.common.config.Configuration;
import jdk.nashorn.internal.runtime.regexp.joni.Config;
import org.google.translate.api.v2.core.Translator;
import org.google.translate.api.v2.core.model.Language;
import org.google.translate.api.v2.core.model.Translation;

import java.util.HashMap;

import static com.amen.common.log.Log.Error;
import static com.amen.common.log.Log.Info;
import com.amen.wiktionary.comparer.gui.ChangeListener;

/**
 * Class represents language. Created by AmeN on 16.02.2016.
 */
public class InternalLanguage {

    public static Language POLISH;
    public static InternalLanguage INTERNAL_POLISH;
    private static Translator sTranslationAPI;
    private static HashMap<String, Language> sList;
    private static boolean loaded = false;

    public static void loadLanguages(ChangeListener pWorker) {
        sTranslationAPI = new Translator("AIzaSyBCaHLk6gaV7UnkJRoc59F3Ezlxt7psrmg");
        pWorker.show("Rozpoczynam pobieranie języków!");
        try {
            Language[] langs = sTranslationAPI.languages("pl");
            sList = new HashMap<>();

            pWorker.show("Do pobrania =>" + langs.length + " języków.");
            for (int i = 0; i < langs.length; i++) {
                sList.put(langs[i].getLanguage(), langs[i]);
                if (langs[i].getLanguage().equals("pl")) {
                    POLISH = langs[i];
                    INTERNAL_POLISH = new InternalLanguage("pl");
                }
                pWorker.show((int) (((double) i / (double) langs.length) * 100.0));
            }
        } catch (Exception e) {
            Error(InternalLanguage.class, "Information about languages couldn't have been found.", e);
            System.exit(1);
        }
        pWorker.show(0);
        pWorker.show("Gotowy!");
        loaded = true;
    }

    // translation of word pronounce in given language
    private String mPronounce;
    // short name of the language
    private String mShortName;
    // MediaWiki language object
    private Language mLang;

    /**
     * Constructor creates an object with language that is valid in context of
     * Google Translate.
     *
     * @param pShortName - short name of that language.
     * @throws Exception - may be thrown if short name is not recognized by
     * google translate api.
     */
    public InternalLanguage(String pShortName) throws Exception {
        mShortName = pShortName;
        mLang = sList.get(pShortName);

        if (mLang == null) {
            throw new Exception("This language hasn't been found in the translation API.");
        }
        if (mShortName.equals("en")) {
            mPronounce = "pronunciation";
        } else {
            try {
                mPronounce = sTranslationAPI.translate("pronunciation", "en", mShortName).getTranslatedText();
            } catch (Exception exc) {
                mPronounce = null;
                Error(getClass(), "Can't find translation for pronunciation words.");
            }
        }
        Info(getClass(), "Translation language added: " + mShortName);

    }

    /**
     * Getter for language short name.
     *
     * @return string with short language name.
     */
    public String getShortName() {
        return mShortName;
    }

    /**
     * Getter for pronunciation.
     *
     * @return pronunciation string.
     */
    public String getPronounce() {
        return mPronounce;
    }

    public Language getmLang() {
        return mLang;
    }

    public static HashMap<String, Language> getsList() {
        return sList;
    }

    /**
     * Translates given word in this language.
     *
     * @param pWord - word to translate.
     * @return word translation. If not available returns null.
     */
    public WordTranslation translate(String pWord) {
        if (loaded) {
            Translation tResult = null;
            try {
                tResult = sTranslationAPI.translate(pWord, "pl", mShortName);
            } catch (Exception e) {
                Error(getClass(), "Translation of " + pWord + " in " + mLang.toString() + " wasn't be found.", e);
                return null;
            }
            Info(getClass(), "Found translation for " + mLang.getLanguage() + ", and it's: " + tResult.getTranslatedText());
            return new WordTranslation(tResult.getTranslatedText(), mLang, this);
        }
        return null;
    }
}
