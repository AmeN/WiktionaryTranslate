package com.amen;

import com.amen.common.ApplicationRuntime;
import com.amen.common.config.ConfigurationFile;
import com.amen.common.db.Database;
import com.amen.wiktionary.comparer.gui.Window;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        ApplicationRuntime.parseApplicationParameters(args);
        ConfigurationFile.load();

        /**
         * Połączenie z bazą danych - na początku pobieram wszystkie informacje dotyczące credential'i z pliku konfiguracyjnego który musi zawierać poniższe
         * wpisy;
         */
        Database.getInstance().setConnectionData(
                ConfigurationFile.getString("db.dbname"),
                ConfigurationFile.getString("db.dbhost"),
                ConfigurationFile.getString("db.dbuser"),
                ConfigurationFile.getString("db.dbpass"));
        Database.getInstance().connect();

        JFrame frame = new JFrame("Wiktionary translations comparer");
        frame.setContentPane(new Window());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
